import Navigation from '../../components/navigation/navigation.component';
import { Button, TextField } from '@material-ui/core';
import styles from './index.module.css';
import type { NextPage } from 'next';
import Head from 'next/head';

const SMS: NextPage = () =>
{
    return (
        <div className="container">
            <Head>
                <title>smsglobal | sms page</title>
            </Head>

            <main>
                <Navigation></Navigation>

                <div className={styles.pageContainer}>
                    <h1>Send an SMS...</h1>
                    <form className={styles.formContainer} noValidate autoComplete='off' onSubmit={handleSubmit}>
                        <TextField
                            variant='filled'
                            margin='normal'
                            id='from'
                            name='from'
                            label='From'
                            required
                            fullWidth
                        />
                        <TextField
                            variant='filled'
                            margin='normal'
                            id='to'
                            name='to'
                            label='To'
                            required
                            fullWidth
                        />
                        <TextField
                            variant='filled'
                            margin='normal'
                            id='message'
                            name='message'
                            label='Message'
                            multiline
                            minRows={2}
                            maxRows={4}
                            required
                            fullWidth
                        />
                        <Button
                            type='submit'
                            variant='contained'
                            color='primary'
                            fullWidth
                        >
                            Send!
                        </Button>
                    </form>
                </div>
            </main>
        </div>
    );
}

export function handleSubmit(event: any)
{
    event.preventDefault();

    // Simple POST request with a JSON body using fetch
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'MAC id="144ab77ebf087138786da5176a1fe673", ts="1649322493", nonce="random-string", mac="MTY0OTMyMjQ5MwpyYW5kb20tc3RyaW5nClBPU1QKL3YyL3NtcwphcGkuc21zZ2xvYmFsLmNvbQo0NDMK"'
        },
        body: JSON.stringify({
            "destination": "0415555921",
            "message": "Hey there!",
            "messages": [],
            "origin": "Unknown"
        })
    };

    fetch('api.smsglobal.com/v2/sms', requestOptions).then(response => console.log(response.json()));
}

export default SMS