import Navigation from '../components/navigation/navigation.component';
import styles from '../styles/Home.module.css';
import type { NextPage } from 'next';
import Head from 'next/head';

const Home: NextPage = () =>
{
    return (
        <div className="container">
            <Head>
                <title>smsglobal code test</title>
            </Head>

            <main>
                <Navigation></Navigation>

                <div className={styles.welcomeContainer}>
                    <h1>Welcome, Please select a tool in the top right.</h1>
                </div>
            </main>
        </div>
    )
}

export default Home
