import Navigation from '../../components/navigation/navigation.component';
import { Button, TextField } from '@material-ui/core';
import styles from './index.module.css';
import type { NextPage } from 'next';
import Head from 'next/head';

const Reports: NextPage = () =>
{
    return (
        <div className="container">
            <Head>
                <title>smsglobal | reports page</title>
            </Head>

            <main>
                <Navigation></Navigation>

                Nothing to see here... 👀
            </main>
        </div>
    );
}

export default Reports