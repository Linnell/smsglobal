import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import type { AppProps } from 'next/app';
import PropTypes from 'prop-types';
import theme from '../src/theme';
import '../styles/globals.css'
import Head from 'next/head';
import React from 'react';

function MyApp({ Component, pageProps }: AppProps)
{
    React.useEffect(() =>
    {
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) jssStyles.parentElement!.removeChild(jssStyles);
    }, []);

    return (
        <React.Fragment>
            <Head>
            <title>SMSGlobal Code Test</title>
            <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
            </Head>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <Component {...pageProps} />
            </ThemeProvider>
        </React.Fragment>
    );
}
export default MyApp

MyApp.propTypes = {
    Component: PropTypes.elementType.isRequired,
    pageProps: PropTypes.object.isRequired,
};