import { Button, Table, TableBody, TableCell, TableHead, TableRow, Paper, TextField } from '@material-ui/core';
import Navigation from '../../components/navigation/navigation.component';
import styles from './index.module.css';
import type { NextPage } from 'next';
import Head from 'next/head';
import { useState } from 'react';

let id = 0;
function createData(apikey: string, secret: string)
{
    id += 1;
    return { id, apikey, secret };
}

const rows = [
    createData('kbl6tu8ajw', '1wkc6qkhex'),
    createData('paat66mprq', 'as4dfdomyg'),
    createData('adfco04n3c', 's4khlie53d'),
    createData('qpgftjnv8f', 'hio2yf8r8l'),
    createData('p2qh45rfib', '065iemq408'),
];

const APIKeySettings: NextPage = () =>
{
    const [resultRows, setResultRows]: any = useState([]);

    function handleSubmit(event: any)
    {
        event.preventDefault();

        rows.push(createData('gvjkkoh2un', 'o1rmbb4e3z'));
        setResultRows(rows);
        console.log(rows);
    }

    return (
        <div className="container">
            <Head>
                <title>smsglobal | api key page</title>
            </Head>

            <main>
                <Navigation></Navigation>

                <div className={styles.pageContainer}>
                    <h1>Save API keys</h1>
                    <form className={styles.formContainer} noValidate autoComplete='off' onSubmit={handleSubmit}>
                        <TextField
                            variant='filled'
                            margin='normal'
                            id='key'
                            name='key'
                            label='Key'
                            required
                            fullWidth
                        />
                        <TextField
                            variant='filled'
                            margin='normal'
                            id='secret'
                            name='secret'
                            label='Secret'
                            required
                            fullWidth
                        />
                        <Button
                            type='submit'
                            variant='contained'
                            color='primary'
                            fullWidth
                        >
                            Add
                        </Button>
                    </form>

                    <br />
                    <br />

                    <Paper>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>API Key</TableCell>
                                    <TableCell align="right">Secret</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.map((row: any) => (
                                    <TableRow key={row.id}>
                                        <TableCell component="th" scope="row">{row.apikey}</TableCell>
                                        <TableCell align="right">{row.secret}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </main>
        </div>
    );
}

export default APIKeySettings