import AssessmentIcon from '@material-ui/icons/Assessment';
import Typography from '@material-ui/core/Typography';
import { Button, Tooltip } from '@material-ui/core';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import SmsIcon from '@material-ui/icons/Sms';
import styles from './navigation.module.css';
import { useRouter } from 'next/router';
import React from 'react';

const Navigation = () =>
{
    const router = useRouter();

    return (
        <React.Fragment>
            <AppBar position="static" color="secondary">
                <Toolbar>
                    <Typography variant="h6" color="inherit" className={styles.grow}>
                        smsglobal
                    </Typography>
                    <Tooltip title="API Key Settings">
                        <Button className={router.pathname == "/api-key-settings" ? styles.navButtonActive : styles.navButton} href="/api-key-settings">
                            <VpnKeyIcon />
                        </Button>
                    </Tooltip>
                    <Tooltip title="SMS">
                        <Button className={router.pathname == "/sms" ? styles.navButtonActive : styles.navButton} href="/sms">
                            <SmsIcon />
                        </Button>
                    </Tooltip>
                    <Tooltip title="Reports">
                        <Button className={router.pathname == "/reports" ? styles.navButtonActive : styles.navButton} href="/reports">
                            <AssessmentIcon />
                        </Button>
                    </Tooltip>
                </Toolbar>
            </AppBar>
        </React.Fragment>
    )
}

export default Navigation