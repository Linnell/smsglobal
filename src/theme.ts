import { Shadows } from "@material-ui/core/styles/shadows"
import { createTheme } from '@material-ui/core/styles';

const theme = createTheme({
    palette: {
        primary: {
            main: '#587CFF',
        },
        secondary: {
            main: '#21201C',
        },
        info: {
            main: '#21201C'
        },
        error: {
            main: '#FB7474',
        },
        background: {
            default: '#FFFFFF',
        },
    },
    typography: {
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        button: {
            textTransform: 'none'
        }
    },
    shadows: Array(25).fill("none") as Shadows,
});

export default theme;